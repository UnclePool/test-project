import Vue from 'vue'
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'
import router from './router'
import 'vuetify/dist/vuetify.min.css'
import './plugins/vuetify'
import store from './store'
import * as firebase from 'firebase/app'

import App from './App.vue'
import BuyModal from './components/Shared/BuyModal'

Vue.component('app-buy-modal', BuyModal)

Vue.use(VueRouter)
Vue.use(Vuetify)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  created () {
    firebase.initializeApp({
      apiKey: 'AIzaSyD4Aa4rgEPSZhkSQP4Bf_1eC_Sp5i62Yd4',
      authDomain: 'testshop-34516.firebaseapp.com',
      databaseURL: 'https://testshop-34516.firebaseio.com',
      projectId: 'testshop-34516',
      storageBucket: 'gs://testshop-34516.appspot.com/',
      messagingSenderId: '509243141248',
      appId: '1:509243141248:web:8acb5bc0214b8c3d'
    })

    firebase.auth().onAuthStateChanged(user => {
      if (user !== null) {
        this.$store.dispatch('saveLogin', user)
      }
    })

    this.$store.dispatch('fetchApp')
  },
  render: h => h(App)
}).$mount('#app')
