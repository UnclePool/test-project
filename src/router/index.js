import Vue from 'vue'
import VueRouter from 'vue-router'
import AuthGuard from './auth-guard'
import Home from '../components/Home'
import Ap from '../components/Apps/Ap'
import AppList from '../components/Apps/AppList'
import NewApp from '../components/Apps/NewApp'
import Login from '../components/Auth/Login'
import Registration from '../components/Auth/Registration'
import Orders from '../components/User/Orders'

Vue.use(VueRouter)

export default new VueRouter({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/app/:id',
      props: true,
      name: 'app',
      component: Ap,
      beforeEnter: AuthGuard
    },
    {
      path: '/list',
      name: 'appList',
      component: AppList,
      beforeEnter: AuthGuard
    },
    {
      path: '/new',
      name: 'newApp',
      component: NewApp,
      beforeEnter: AuthGuard
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/registration',
      name: 'registration',
      component: Registration
    }
  ],
  mode: 'history'
})
