import * as firebase from 'firebase/app'
import 'firebase/database'
import 'firebase/storage'

class App {
  constructor (
    title,
    description,
    imgSrc = '',
    promo = false,
    id = null
  ) {
    this.title = title
    this.description = description
    this.imgSrc = imgSrc
    this.promo = promo
    this.id = id
  }
}

export default {
  state: {
    apps: []
  },
  mutations: {
    createApp (state, payload) {
      state.apps.push(payload)
    },
    loadApps (state, payload) {
      state.apps = payload
    }
  },
  actions: {
    async createApp ({ commit }, payload) {
      commit('setClear')
      commit('setLoading', true)

      const image = payload.image

      try {
        const newApp = new App(
          payload.title,
          payload.description,
          '',
          payload.promo,
          payload.id
        )

        const app = await firebase.database().ref('apps').push(newApp)
        const imageExt = image.name.slice(image.name.lastIndexOf('.'))

        const fileData = await firebase.storage().ref(`app/${app.key}${imageExt}`).put(image)
        const imgSrc = await fileData.ref.getDownloadURL()

        await firebase.database().ref('apps').child(app.key).update({ imgSrc })

        commit('setLoading', false)
        commit('createApp', {
          ...newApp,
          id: app.key,
          imgSrc
        })
      } catch (error) {
        commit('setError', error.message)
        commit('setLoading', false)
        throw error
      }
    },
    async fetchApp ({ commit }) {
      commit('setClear')
      commit('setLoading', true)
      const resultApps = []
      try {
        const fbVal = await firebase.database().ref('apps').once('value')
        const apps = fbVal.val()
        Object.keys(apps).forEach(key => {
          const app = apps[key]
          resultApps.push(new App(app.title, app.description, app.imgSrc, app.promo, key))
        })
        commit('loadApps', resultApps)
        commit('setLoading', false)
      } catch (e) {
        commit('setError', e.message)
        commit('setLoading', false)
        throw e
      }
    }
  },
  getters: {
    app (state) {
      return state.apps
    },
    onPromo (state) {
      return state.apps.filter(elem => elem.promo)
    },
    myApp (state) {
      return state.apps
    },
    appById: state => appId => {
      return state.apps.find(app => app.id == appId)
    }
  }
}
