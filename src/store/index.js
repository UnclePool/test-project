import Vue from 'vue'
import Vuex from 'vuex'
import ap from './ap'
import user from './user'
import shared from './shared'
import order from './order'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    ap,
    user,
    shared,
    order
  }
})
